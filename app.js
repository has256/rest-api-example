/**
 * @fileOverview Server Creation in ExpressJS!
 * @author Carlos EA. Batista
 * @version 1.0.0
 */

const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const lessMiddleware = require('less-middleware');
const swaggerUi = require('swagger-ui-express'),
			swaggerDocument = require('./swagger.json');

require('./server/models/database');

let app = express();
/*var basicAuth = require('express-basic-auth');*/
const index = require('./server/routes/allRoutes');

/* include the views and set jade(RIP) to view engine */
app.set('views', path.join(__dirname, 'server', 'views'));
app.set('view engine', 'jade');

/* basic auth to the Rest API 
app.use(basicAuth({ users: { 'carduzera': 'psswrd' } } ) );  */

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/', index);
app.use('/users', index);

app.use(function(request, response, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

app.use(function(err, request, response, next) {
	response.locals.message = err.message;
	response.locals.error = request.app.get('env') === 'development' ? err : {};
	response.status(err.status || 500);
	response.render('error');
});

const port = app.get('port') ||	3000;
app.listen(port, () => console.log('Server is listening on port ' + port));

module.exports = app;