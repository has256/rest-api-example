# melissaAPI()

melissaAPI was created by Carlos EA. Batista <Cardu> to you understand a REST API example. 

![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg) ![forthebadge](http://forthebadge.com/images/badges/uses-js.svg)

![forthebadge](https:mg.shields.io/badge/node-v4.2.6-blue.svg)
![forthebadge](https://img.shields.io/badge/npm-v3.2.2-blue.svg)
![forthebadge](https://img.shields.io/badge/platform-linux--64-brightgreen.svg)
![forthebadge](https://img.shields.io/badge/contributions-welcome-orange.svg)

## How to run local

1. Certify that you have installed in your machine
	- [CLI](https://git-for-windows.github.io/)
	- [node.js](https://nodejs.org/en/)
	- [npm](https://www.npmjs.com/)

2. In your CLI, use the following commands!
   
```bash
# Clone this repository
$ git clone https://github.com/el-unicorn/rest-api-example

# Go into the repository
$ cd rest-api-example

# Install dependencies
$ npm install

# Run the app
$ npm start
```

1. To test the routes, use [Postman](https://www.getpostman.com/).
2. In the Postman, use the following data to basic authentication for the Rest API

| Username  | Password |
| ------------- | ------------- |
| carduzera  | thevelops  |

5. You can use the following routes!
> Tip: use the application/x-www-form-unlercoded to the POST and PUT methods.

| Method  | Route | Function | Interface Avaliable? |
| ------------- | ------------- | ----------------- | ------------- |
| GET  | /  | Return an API homepage in HTML | YES |
| POST  | /fakeusers  | Populates the db with 50 fake users | NOT YET |
| GET  | /users  | Return an array of JSON with users | YES |
| POST  | /users  | Create a new user | YES |
| DELETE  | /users  | Delete all users in the db | NOT YET |
| GET  | /users/:id  | Return a specific user  | NOT YET |
| PUT  | /users/:id  | Update a specific user | NOT YET |
| DELETE  | /users/:id  | Delete a specific user | NOT YET |
| GET | /about | Return some project infos in HTML  | YES |
| GET | /docs | Return Swagger Documentation  | NOT YET |

To access the routes avaliable in a bootstrap static page on the browser, it's necessary edit the [app.js](https://github.com/el-unicorn/rest-api-example/blob/master/app.js), commenting this part of code

```javascript
app.use(basicAuth({ users: { 'carduzera': 'psswrd' } } ) );
```
like this

```javascript
//app.use(basicAuth({ users: { 'carduzera': 'psswrd' } } ) );
```
...and later, restart the server and open your browser in the URI: http://localhost:3000/

1. An error can cause API crash, so if something happens, just restart the API with npm start again.

## Documentation

### JSDoc

The code is documented using [JSDoc]( http://usejsdoc.org) patterns - but none HTML page was generated in /out directories. But, if it's necessary, just follow this steps.

```bash
# Install JSDoc globally
$ sudo npm install -g JSDoc

# Execute command jsdoc to all files
$ jsdoc exampleFileName.js
```

### Swagger Documentation

The API is documented using [Swagger](https://swagger.io/), to access the documentation, just use the endpoint:
- /docs

## References and Tools

Things that helped me a lot to develop this!

### Books

* FLANAGAN, David. JavaScript: the definitive guide. " O'Reilly Media, Inc.", 2006.
* HOLMES, Simon. Getting MEAN with Mongo, Express, Angular, and Node. Manning Publications Co., 2015.

### Other Tools

* [CLI](https://git-for-windows.github.io/)
* [npm](https://www.npmjs.com/)
* [node.js](https://nodejs.org/en/)
* [Atom](https://atom.io/)
* [Postman](https://www.getpostman.com/)
* [Firefox](https://www.mozilla.org/pt-BR/firefox/new/)
* [Mongo Lab](https://mlab.com/)
* [Github](https://github.com/)
* [Markdown Editor](https://jbt.github.io/markdown-editor/)
* A lot of codes/articles/blogs/videos, sorry don't compile.

## Author

Carlos EA. Batista (abreu.carlos@aluno.ufabc.edu.br)

## License

[![forthebadge](http://forthebadge.com/images/badges/cc-by.svg)](http://forthebadge.com)