/**
 * @fileOverview Contains the list with all API Controllers related to Pages Renders!
 * @author Carlos EA. Batista
 * @version 1.0.0
 */

module.exports = {

	index: async(request, response) => {
		response.status(200).render('home');
	},

	aboutThis: async(request, response) => {
		response.status(200).render('about', { title: 'About'});
	},

};
