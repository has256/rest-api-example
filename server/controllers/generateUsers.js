/**
 * @fileOverview Contains the controller that populates the	db with 50 fake users
 * @author Carlos EA. Batista
 * @version 1.0.0
 */

const faker = require('faker/locale/pt_BR');

let mongoose = require('mongoose'),
		User = mongoose.model('User');


module.exports = {

	createFakeUser: async(request, response) => {

		let numberMinOfUsers = 50;
		for (let numberOfUsers = 1; numberOfUsers <= numberMinOfUsers; numberOfUsers++) {

			let fakeEmail = faker.internet.email();
			let fakeFirstName = faker.name.firstName();
			let fakeLastName = faker.name.lastName();
			let fakePersonalPhone = faker.phone.phoneNumberFormat(1);

			const fakeAnUser = new User({email: fakeEmail, firstName: fakeFirstName, lastName: fakeLastName, personalPhone: fakePersonalPhone});
			const user = await fakeAnUser.save();
		};

		response.status(201).json({status: "Everything is OK!"});
	},

};
