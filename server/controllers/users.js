/**
 * @fileOverview Contains the list with all API Controllers related to Users!
 * @author Carlos EA. Batista
 * @version 1.0.0
 */

const mongoose = require('mongoose'),
			User = mongoose.model('User');

module.exports = {

	readAllUsers: async(request, response) => {
		const users = await User.find({});
		response.status(200).json(users);
	},

	deleteAllUsers: async(request, response) => {
		const user = await User.remove({});
		response.status(201).json({ message: 'All Users deleted successfully!'});
	},

	createNewUser: async(request, response) => {
		const newUser = new User(request.body);
		const user = await newUser.save();
		response.status(201).json(user);
	},

	readUserById: async(request, response) => {
		const user = await User.findById(request.params.id);
		response.status(201).json(user);
	},

	updateUserById: async(request, response) => {
		const user = await User.findOneAndUpdate(request.params.id, request.body, {new: true});
		response.status(201).json(user);
	},

	deleteUserById: async(request, response) => {
		const user = await User.findOneAndRemove(request.params.id);
		response.status(201).json({ message: 'User deleted successfully!'});
	},

};
