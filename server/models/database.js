/**
 * @fileOverview Responsible to connect with a database (mLab)!
 * @author Carlos EA. Batista
 * @version 1.0.0
 */

let mongoose = require('mongoose');
let databaseURI = process.env.DB_CONNECTION_STRING;
mongoose.connect(databaseURI);

mongoose.connection.on('connected', function () {
	console.log('Mongoose connected to ' + databaseURI);
});

mongoose.connection.on('error', function (err) {
	console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
	console.log('Mongoose disconnected');
});

require('./usersModel');
