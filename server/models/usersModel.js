/**
 * @fileOverview Contains the models of objects stored in the database!
 * @author Carlos EA. Batista
 * @version 1.0.0
 */


let mongoose = require('mongoose');
let Schema = mongoose.Schema;

/**
*  Scheme from an user!
* @constructor
* @param {string} email - Contains an users email!
* @param {string} firstName -  Contains an users first name!
* @param {string} lastName - Contains an users last name!
* @param {string} personalPhone  - Contains an users personal phone!
* @param {boolean} versionKey - Contains the internal revision of the document. 'Cause I don't need this, it's false.
*/


const usersModel = new Schema({
	firstName: {type: String, required: "Please enter your first name."},
	lastName: {type: String, required: 'Hey, you forgot your last name.'},
	email: {type: String, required: "Email is required. I swear, we don't send spam!"},
	personalPhone: {type: String, required: 'Ooops, phone number missing! '}
}, {
	versionKey: false
});

module.exports = mongoose.model('User', usersModel);