/**
 * @fileOverview Contains the list with all RestAPI routes!
 * @author Carlos EA. Batista
 * @version 1.0.0
 */

const express = require('express');
const router = require('express-promise-router')();

const ctrlPages = require('../controllers/pages');
const ctrlUsers = require('../controllers/users');
const ctrlFakeUsers = require('../controllers/generateUsers');

router.route('/')
	.get(ctrlPages.index);

router.route('/users')
	.get(ctrlUsers.readAllUsers)
	.post(ctrlUsers.createNewUser)
	.delete(ctrlUsers.deleteAllUsers);

router.route('/fakeusers')
	.post(ctrlFakeUsers.createFakeUser);

router.route('/users/:id')
	.get(ctrlUsers.readUserById)
	.put(ctrlUsers.updateUserById)
	.delete(ctrlUsers.deleteUserById);

router.route('/about')
	.get(ctrlPages.aboutThis);

module.exports = router;
